﻿using UnityEngine;
using System.Collections;

public class Ptenec : MonoBehaviour {
    public float speed = 100;
    private Vector2 direction;

	void Start () {
        if(GetComponent<Renderer>().isVisible) {
            var randomPointInView = new Vector3(Random.Range(0,Screen.width), Random.Range(0,Screen.height), 0);
            var destination = Camera.main.ScreenToWorldPoint(randomPointInView);
            direction = destination - transform.position;
        }
        else {
            var x = Random.Range(-1f, 1f);
            var y = Random.Range(0.3f, 1f);
            direction = new Vector2(x, y);
        }
        direction.Normalize();
	    Destroy(gameObject, 15.0f);
	}
	
	// Update is called once per frame
	void Update () {
	    transform.Translate(direction * speed * Time.deltaTime);
	}
}
