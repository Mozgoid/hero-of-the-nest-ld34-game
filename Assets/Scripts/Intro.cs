﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour {
    public GameObject[] slides;
    public GameObject gui;
    private int current = 0;

	void Start () {
	    Time.timeScale = 0;
	}

    void Update() {
        if(Input.anyKeyDown) {
            Next();
        }
    }
	
    public void Next() {
        if(current >= slides.Length)
            return;

        if(current >= 0) {
            slides[current].SetActive(false);
        }
        current++;
        if(current >= slides.Length) {
            Time.timeScale = 1;
            gameObject.SetActive(false);
            gui.SetActive(true);
        }
        else {
            slides[current].SetActive(true);
        }
    }
}
