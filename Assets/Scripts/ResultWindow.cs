﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResultWindow : MonoBehaviour {

    [System.Serializable]
    public class ResultCase {
        public string text;
        public string button;
        public int alives;
    }

    public ResultCase[] results;
    public Text text;
    public Text buttonText;
    public Text counter;

    public void Init(int alive) {
        counter.text = alive.ToString();
        int i = -1;
        foreach (var r in results) {
            if (r.alives > alive) {
                break;
            }
            i++;
        }
        buttonText.text = results[i].button;
        text.text = results[i].text;
    }
}
