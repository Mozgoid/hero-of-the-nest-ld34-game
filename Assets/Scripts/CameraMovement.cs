﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public Camera targetCamera;
    public Vector3 startPos;
    public float startSize;
    public float timeToMove;

    private Vector3 endPos;
    private float endSize;
    private Vector3 moveSpeed;
    private float sizeSpeed;

	void Start () {
        endPos = targetCamera.transform.position;
	    endSize = targetCamera.orthographicSize;
        sizeSpeed = (endSize - startSize) / timeToMove;
        moveSpeed = (endPos - startPos) / timeToMove;
        targetCamera.orthographicSize = startSize;
        targetCamera.transform.position = startPos;
	}

	void Update () {
        if(timeToMove < 0)
            return;

	    timeToMove -= Time.deltaTime;
        if(timeToMove > 0) {
            targetCamera.orthographicSize += sizeSpeed * Time.deltaTime;
            targetCamera.transform.Translate(moveSpeed * Time.deltaTime);
        }
        else {
            targetCamera.orthographicSize = endSize;
            targetCamera.transform.position = endPos;
        }
	}
}
