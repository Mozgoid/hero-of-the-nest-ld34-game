﻿using UnityEngine;
using System.Collections;

public class Chicken : MonoBehaviour {

	//public float newEggPeriod = 10.0f;
	public GameObject eggPrefab;

	private Vector2 deltaToEgg;
	public float timeToEgg;
	private AudioSource bornSound;

	void Start () {
		deltaToEgg = eggPrefab.transform.position - transform.position;        
		bornSound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		timeToEgg -= Time.deltaTime;
		if (timeToEgg < 0) {
			CreateNewEgg();
		}

	}

	void CreateNewEgg() {
		var newEgg = Instantiate (eggPrefab) as GameObject;
		newEgg.transform.SetParent (transform.parent);
		newEgg.transform.position = (Vector2)transform.position + deltaToEgg;
		newEgg.SetActive (true);
		timeToEgg = Random.Range(6.5f, 9.0f);//newEggPeriod;
        bornSound.Play();
	}

	public void OnEggEnd(Egg egg) {
		// var t = timeToEgg;
		// if(egg.dead) {
		// 	CreateNewEgg();
		// 	timeToEgg = t;
		// }
	}
}
