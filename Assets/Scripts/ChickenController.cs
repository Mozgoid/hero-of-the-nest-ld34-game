using UnityEngine;
using System.Collections;

public class ChickenController : MonoBehaviour {

	Rigidbody2D body;
	public float force = 10.0f;

	void Start () {
		body = GetComponent<Rigidbody2D> ();
	}

	void Update () {
		body.AddForce (transform.right * Input.GetAxis ("Horizontal") * force); 
	}
}
