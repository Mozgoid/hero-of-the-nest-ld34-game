﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AddForce : MonoBehaviour {

    public Rigidbody2D target;
    public bool guiPressed {get; set;}
    public float force = 100;
    public KeyCode key;
    public Button button;
    ColorBlock normalColors;

	// Use this for initialization
	void Start () {
        normalColors = button.colors;
	}
	
	// Update is called once per frame
	void Update () {
        var keyPressed = Input.GetKey(key);
        if(keyPressed || guiPressed) {
            AddUpForce();
        }
        if(!guiPressed) {
            ForceColoring(keyPressed);
        }
	}

    void ForceColoring(bool force) {
        if(force) {
            button.image.color = normalColors.pressedColor;
        } else {
            button.image.color = normalColors.normalColor;
        }
    }
    public void AddUpForce() {
        target.AddForceAtPosition(transform.up*force, transform.position);
    }
}
