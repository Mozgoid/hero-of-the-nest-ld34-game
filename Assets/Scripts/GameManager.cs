﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {
	public Text alive;
	public Text dead;
    public Text tonext;
    public ResultWindow results;
	public GameObject ptenec;
    public Chicken chicken;
    public GameObject brokenEgg;
    public int winCount = 12;

	private int aliveCounter;
	public int deadLimit = 5;

	public void EggResult(Egg oldegg) {
		if (!oldegg.dead) {
			aliveCounter++;
			alive.text = aliveCounter.ToString();
            CreatePtenec(oldegg);
		} else {
			deadLimit--;
			dead.text = deadLimit.ToString ();
		}
        chicken.OnEggEnd(oldegg);
        Destroy(oldegg.gameObject);
        if (deadLimit <= 0 || aliveCounter >= winCount) {
            EndGame();
        }
	}

    void CreatePtenec(Egg oldegg) {
        var newPtenec = Instantiate(ptenec) as GameObject;
        newPtenec.transform.position = oldegg.transform.position;
        newPtenec.gameObject.SetActive(true);

        if(brokenEgg == null)
            return;

        var wrecked = Instantiate(brokenEgg,
            oldegg.transform.position,
            oldegg.transform.rotation) as GameObject;

        var eggRigidBody = oldegg.GetComponent<Rigidbody2D>();
        var bodies = wrecked.GetComponentsInChildren<Rigidbody2D>();
        foreach (Rigidbody2D body in bodies) {
            body.AddForce(eggRigidBody.velocity);
        }
        Destroy(wrecked, 5.0f);
    }

	// Use this for initialization
	void Start () {
        results.gameObject.SetActive(false);
        dead.text = deadLimit.ToString ();
	}

	void Update () {
        tonext.text = chicken.timeToEgg.ToString("0.00");
	}

    public void EndGame() {
        Time.timeScale = 0;
        results.Init(aliveCounter);
        results.gameObject.SetActive(true);
    }

    public void Restart() {
        Application.LoadLevel("1");
        Time.timeScale = 1;
    }
}
