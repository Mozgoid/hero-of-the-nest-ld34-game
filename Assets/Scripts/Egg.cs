﻿using UnityEngine;
using System.Collections;

public class Egg : MonoBehaviour {
	
	public GameManager manager;
	public float timeToBorn = 10.0f;
	public bool dead;

	void Update () {
		timeToBorn -= Time.deltaTime;
		if (timeToBorn < 0) {
			End(true);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "ground") {
			End (false);
		}		
	}

	void End(bool success) {
		if(dead)
			return;

		dead = !success;
		manager.EggResult (this);
	}
}
